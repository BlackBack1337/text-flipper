package ru.blackCoder.txtflipper.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * Created by BlackCoder on 08.01.2016.
 */

// Если вы собираетесь это читать - то не надо, юзайте готовый билд :3

public class FlipperFrame extends JFrame
{
    JScrollPane scroll_from;
    JTextArea area_from;
    JScrollPane scroll_to;
    JTextArea area_to;
    JButton btn_go;
    JCheckBox check_clip;
    JLabel label_clip;
    JLabel info;

    public FlipperFrame()
    {
        init();
        new Thread(
                new Runnable() {
                    @Override
                    public void run()
                    {
                        while (true)
                        {
                            if (check_clip.isSelected())
                            {
                                try {
                                    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(reverseString(Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null).getTransferData(DataFlavor.stringFlavor).toString())), null);
                                } catch (UnsupportedFlavorException | IOException e) {
                                    JOptionPane.showMessageDialog(null, "Ошибка при работе с Clipboard`ом...", "Text Flipper - Clipboard Error", JOptionPane.ERROR_MESSAGE);
                                    e.printStackTrace();
                                }
                            }
                            try {
                                Thread.sleep(300);
                            } catch (InterruptedException e) {
                                // Видимо процесс проснулся раньше времени, ничего страшного...
                                e.printStackTrace();
                            }
                        }
                    }
                }
        ).start();
    }

    private void init()
    {
        setTitle("Text Flipper - by BlackCoder");
        setSize(new Dimension(500, 300));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLayout(null);
        setLocationRelativeTo(null);
        setResizable(false);

        area_from = new JTextArea();
        scroll_from = new JScrollPane(area_from);
        scroll_from.setBounds(5,5, 485, 100);

        area_to = new JTextArea();
        scroll_to = new JScrollPane(area_to);
        scroll_to.setBounds(5, 140, 485, 100);

        btn_go = new JButton("Flip");
        btn_go.setBounds(5, 110, 237, 25);

        check_clip = new JCheckBox();
        check_clip.setBounds(247, 110, 25, 25);

        label_clip = new JLabel("Use Clipboard? (Auto Text Flipping)");
        label_clip.setBounds(277, 110, 200, 25);

        info = new JLabel("Данная программа была написана по приколу, никакой смысловой нагрузки :3");
        info.setBounds(0, 240, 490, 25);
        info.setHorizontalAlignment(SwingConstants.CENTER);

        addListeners();

        add(scroll_from);
        add(scroll_to);
        add(btn_go);
        add(info);
        add(check_clip);
        add(label_clip);
        setVisible(true);
    }

    private void addListeners()
    {
        btn_go.addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e)
                    {
                        area_to.setText(reverseString(area_from.getText().toLowerCase()));
                    }
                }
        );
    }

    public String reverseString(String str)
    {
        // Более хитрого способа я не придумал, другие слишком запарные...
        return new String(new StringBuffer(
                            str.replace("а", "ɐ").replace("б", "ƍ").replace("в", "ʚ")
                            .replace("г", "ɹ").replace("д", "ɓ").replace("е", "ǝ").replace("ё", "ǝ")
                            .replace("з", "ε").replace("к", "ʞ").replace("л", "v").replace("м", "w")
                            .replace("н", "н").replace("п", "u").replace("р", "d").replace("с", "ɔ")
                            .replace("т", "ɯ").replace("у", "ʎ").replace("ф", "ȸ").replace("ц", "ǹ")
                            .replace("ч", "Һ").replace("ш", "m").replace("щ", "m").replace("ь", "q")
                            .replace("ы", "ıq").replace("ъ", "q").replace("э", "є").replace("ю", "oı")
                            .replace("я", "ʁ")
                            .replace("a", "ɐ").replace("b", "q").replace("c", "ɔ").replace("d", "p")
                            .replace("e", "ǝ").replace("f", "ɟ").replace("g", "ƃ").replace("h", "ɥ")
                            .replace("i", "ı").replace("k", "ʞ").replace("l", "l").replace("m", "ɯ")
                            .replace("n", "u").replace("o", "o").replace("p", "d").replace("q", "ᕹ")
                            .replace("r", "ɹ").replace("s", "s").replace("t", "ʇ").replace("v", "ʌ")
                            .replace("x", "x").replace("y", "ʎ").replace("z", "z")
                            .replace("1", "Ɩ").replace("2", "ᄅ").replace("3", "Ɛ").replace("4", "ㄣ")
                            .replace("5", "ϛ").replace("6", "9").replace("7", "ㄥ").replace("8", "8")
                            .replace("9", "6").replace("!", "¡").replace("?", "¿")
                                ).reverse());
    }
}
