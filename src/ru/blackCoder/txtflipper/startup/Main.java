package ru.blackCoder.txtflipper.startup;

import ru.blackCoder.txtflipper.gui.FlipperFrame;

/**
 * Created by BlackCoder on 08.01.2016.
 */

public class Main {

    public static void main(String[] args)
    {
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                new FlipperFrame();
            }
        }).start();
    }

}
